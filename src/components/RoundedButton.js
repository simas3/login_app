import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
  Dimensions
} from "react-native";
import PropTypes from "prop-types";
import colors from "./colors";

export default class RoundedButton extends Component {
  render() {
    const {
      text,
      textColor,
      background,
      handleOnPress,
      loading,
      disabled,
      textSize,
      textWeight,
      textAlign,
      borderColor
    } = this.props;
    const backgroundColor = background || "transparent";
    const color = textColor || colors.black;
    const fontSize = textSize || 16;
    const fontWeight = textWeight || "600";
    const alignPosition = textAlign || "center";
    const border = borderColor || colors.white;
    const opacityStyle = disabled || loading ? 0.5 : 1;
    const textOpacity = loading ? 0 : 1;
    return (
      <TouchableOpacity
        onPress={handleOnPress}
        style={[
          styles.wrapper,
          {
            opacity: opacityStyle,
            backgroundColor,
            borderColor: border
          }
        ]}
        disabled={disabled || loading}
      >
        {loading ? (
          <ActivityIndicator size="large" color="white" />
        ) : (
          <View style={styles.buttonTextWrapper}>
            <Text
              style={[
                styles.buttonText,
                {
                  opacity: textOpacity,
                  color,
                  textAlign: alignPosition,
                  fontWeight,
                  fontSize
                }
              ]}
            >
              {text}
            </Text>
          </View>
        )}
      </TouchableOpacity>
    );
  }
}

RoundedButton.propTypes = {
  text: PropTypes.string.isRequired,
  textColor: PropTypes.string,
  backgroundColor: PropTypes.string,
  icon: PropTypes.object,
  textSize: PropTypes.string,
  textWeight: PropTypes.string,
  textAlign: PropTypes.string,
  iconPosition: PropTypes.string,
  borderColor: PropTypes.string,
  disabled: PropTypes.bool.isRequired,
  loading: PropTypes.bool.isRequired,
  handleOnPress: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
  wrapper: {
    width: Dimensions.get("window").width - 60,
    height: 50,
    justifyContent: "center",
    marginBottom: 15,
    alignItems: "center",
    borderRadius: 40,
    borderWidth: 1
  },
  buttonTextWrapper: {
    flexDirection: "row",
    justifyContent: "center"
  },
  buttonText: {
    fontSize: 17,
    width: "100%"
  }
});
