import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Animated,
  Easing,
  Dimensions
} from "react-native";
import PropTypes from "prop-types";
import colors from "./colors";
import Icon from "react-native-vector-icons/dist/FontAwesome";

class InputField extends Component {
  constructor(props) {
    super(props);
    this.state = {
      secureInput:
        props.inputType === "text" || props.inputType === "email"
          ? false
          : true,
      scaleCheckmarkValue: new Animated.Value(0)
    };
  }

  toggleShowPassword() {
    this.setState({ secureInput: !this.state.secureInput });
  }

  scaleCheckmark(value) {
    Animated.timing(this.state.scaleCheckmarkValue, {
      toValue: value,
      duration: 400,
      easing: Easing.easeOutBack,
      useNativeDriver: true
    }).start();
  }

  render() {
    const {
      labelText,
      labelTextSize,
      labelTextWeight,
      labelColor,
      textColor,
      borderBottomColor,
      inputStyle,
      inputType,
      onChangeText,
      customStyle,
      showCheckmark,
      autoCapitalize,
      autoFocus,
      value,
      placeholder
    } = this.props;
    const { secureInput, scaleCheckmarkValue } = this.state;
    const fontSize = labelTextSize || 14;
    const fontWeight = labelTextWeight || "700";
    const color = labelColor || colors.white;
    const inputColor = textColor || colors.white;
    const borderBottom = borderBottomColor || "transparent";
    const keyboardType = inputType === "email" ? "email-address" : "default";
    let customInputStyle = inputStyle || {};
    if (!inputStyle || (inputStyle && !inputStyle.paddingBottom)) {
      customInputStyle.paddingBottom = 5;
    }

    const iconScale = scaleCheckmarkValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [0.01, 1.6, 1]
    });
    const scaleValue = showCheckmark ? 1 : 0;
    this.scaleCheckmark(scaleValue);

    return (
      <View style={[styles.wrapper, customStyle]}>
        <Text style={[styles.label, { fontWeight, fontSize, color }]}>
          {labelText}
        </Text>
        {inputType === "password" ? (
          <TouchableOpacity
            style={styles.showButton}
            onPress={this.toggleShowPassword.bind(this)}
          >
            <Text style={styles.showButtonText}>
              {secureInput ? "Show " : "Hide "}
            </Text>
          </TouchableOpacity>
        ) : null}
        <Animated.View
          style={[
            { transform: [{ scale: iconScale }] },
            styles.checkmarkWrapper
          ]}
        >
          <Icon name="check" color={colors.white} size={20} />
        </Animated.View>
        <TextInput
          style={[
            styles.inputField,
            { color: inputColor, borderBottomColor: borderBottom },
            customInputStyle
          ]}
          secureTextEntry={secureInput}
          onChangeText={onChangeText}
          keyboardType={keyboardType}
          autoFocus={autoFocus}
          autoCapitalize={autoCapitalize}
          autoCorrect={false}
          placeholder={placeholder || ""}
          value={value}
        />
      </View>
    );
  }
}

InputField.propTypes = {
  labelText: PropTypes.string.isRequired,
  labelTextSize: PropTypes.number,
  labelColor: PropTypes.string,
  textColor: PropTypes.string,
  borderBottomColor: PropTypes.string,
  inputType: PropTypes.string.isRequired,
  customStyle: PropTypes.object,
  onChangeText: PropTypes.func,
  showCheckmark: PropTypes.bool.isRequired,
  autoFocus: PropTypes.bool,
  autoCapitalize: PropTypes.string,
  labelTextWeight: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  inputStyle: PropTypes.object
};

const styles = StyleSheet.create({
  wrapper: {},
  label: {
    marginBottom: 10
  },
  inputField: {
    fontSize: 18,
    borderBottomWidth: 1,
    width: Dimensions.get("window").width - 60,

    borderBottomColor: colors.white
  },
  showButton: {
    position: "absolute",
    right: 0
  },
  showButtonText: {
    color: colors.white,
    fontWeight: "700"
  },
  checkmarkWrapper: {
    position: "absolute",
    right: 0,
    bottom: 12
  }
});

export default InputField;
