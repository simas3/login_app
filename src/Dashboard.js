import React, { Component } from "react";
import { Text, View, Image, Dimensions, ActivityIndicator } from "react-native";
import LinearGradient from "react-native-linear-gradient";

export default class Dashboard extends Component {
  state = { loading: false };
  render() {
    const {
      user: { image, firstName, lastName, address, phone }
    } = this.props.navigation.state.params;
    return (
      <LinearGradient
        style={{ flex: 1 }}
        start={{ x: 0, y: 1 }}
        end={{ x: 0, y: 0 }}
        colors={["#ff5f6d", "#ffc371"]}
      >
        <View
          style={{
            flex: 1,
            marginTop: 120,
            alignItems: "center"
          }}
        >
          {this.state.loading ? (
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                width: Dimensions.get("window").width - 80,
                height: Dimensions.get("window").width - 80
              }}
            >
              <ActivityIndicator color="white" size="large" />
            </View>
          ) : null}

          <Image
            resizeMode="contain"
            source={{ uri: image }}
            onLoad={() => this.setState({ loading: true })}
            onLoadEnd={() => this.setState({ loading: false })}
            style={{
              width: Dimensions.get("window").width - 80,
              height: Dimensions.get("window").width - 80
            }}
          />

          <View
            style={{
              width: Dimensions.get("window").width - 80,

              marginTop: 20,
              borderWidth: 1,
              borderColor: "white",
              borderRadius: 15,
              justifyContent: "center",
              alignItems: "center",
              paddingHorizontal: 20
            }}
          >
            <Text
              style={{
                fontSize: 16,
                color: "white",
                textAlign: "center",
                marginTop: 10
              }}
            >
              {firstName + " " + lastName}
            </Text>
            <Text
              style={{
                fontSize: 16,
                color: "white",
                textAlign: "center",
                marginTop: 10
              }}
            >
              {address}
            </Text>
            <Text
              style={{
                fontSize: 16,
                color: "white",
                textAlign: "center",
                marginTop: 10,
                marginBottom: 10
              }}
            >
              {phone}
            </Text>
          </View>
        </View>
      </LinearGradient>
    );
  }
}
