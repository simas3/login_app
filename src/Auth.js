import React, { Component } from "react";
import { Image, Dimensions, View, StyleSheet } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { login } from "./api";
import LinearGradient from "react-native-linear-gradient";
import RoundedButton from "./components/RoundedButton";
import colors from "./components/colors";
import InputField from "./components/InputField";
import axios from "axios";
const SCREEN_WIDTH = Dimensions.get("window").width;
export default class Auth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formValid: true,
      validUsername: false,
      username: "",
      password: "",
      validPassword: false,
      loading: false
    };
  }

  handleUsernameChange = username => {
    this.setState({ username });
    if (!this.state.validUsername) {
      if (username.length > 4) {
        this.setState({ validUsername: true });
      }
    } else if (username.length <= 4) {
      this.setState({ validUsername: false });
    }
  };

  handlePasswordChange = password => {
    this.setState({ password });
    if (!this.state.validPassword) {
      if (password.length > 4) {
        this.setState({ validPassword: true });
      }
    } else if (password.length <= 4) {
      this.setState({ validPassword: false });
    }
  };

  onSubmitPress = () => {
    this.setLoading();
    const { navigate } = this.props.navigation;
    const { username, password } = this.state;
    login(username, password, navigate, this.setLoading, this.clearFields);
  };

  clearFields = () => {
    this.setState({
      username: "",
      password: "",
      validPassword: false,
      validUsername: false
    });
  };

  setLoading = () => {
    this.setState(prevState => ({ loading: !prevState.loading }));
  };

  render() {
    const {
      loading,
      validUsername,
      validPassword,
      username,
      password
    } = this.state;

    return (
      <LinearGradient
        style={{ flex: 1 }}
        start={{ x: 0, y: 1 }}
        end={{ x: 0, y: 0 }}
        colors={["#ff5f6d", "#ffc371"]}
      >
        <KeyboardAwareScrollView
          style={{ flex: 1 }}
          resetScrollToCoords={{ x: 0, y: 0 }}
          contentContainerStyle={{ alignItems: "center" }}
          scrollEnabled={false}
          enableOnAndroid
          enableResetScrollToCoords={true}
          extraHeight={200}
          keyboardShouldPersistTaps="handled"
        >
          <View style={styles.imageContainer}>
            <Image
              style={styles.image}
              source={{ uri: "https://placeimg.com/80/80/tech" }}
            />
          </View>
          <InputField
            labelText="USERNAME"
            labelTextSize={14}
            labelColor={colors.white}
            textColor={colors.white}
            borderBottomColor={colors.white}
            inputType="email"
            onChangeText={this.handleUsernameChange}
            customStyle={{ marginBottom: 20 }}
            showCheckmark={validUsername}
            autoCapitalize={"none"}
            value={username}
          />
          <InputField
            labelText="PASSWORD"
            labelTextSize={14}
            labelColor={colors.white}
            textColor={colors.white}
            borderBottomColor={colors.white}
            inputType="password"
            customStyle={{ marginBottom: 20 }}
            onChangeText={this.handlePasswordChange}
            showCheckmark={validPassword}
            autoCapitalize={"none"}
            value={password}
          />
          <RoundedButton
            loading={loading}
            disabled={!validPassword || !validUsername}
            textColor={colors.white}
            text="Submit"
            handleOnPress={this.onSubmitPress}
          />
        </KeyboardAwareScrollView>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  imageContainer: {
    height: SCREEN_WIDTH * 0.33,
    width: SCREEN_WIDTH * 0.33,
    borderRadius: 14,
    marginTop: 100,
    marginBottom: 20
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode: "contain"
  }
});
