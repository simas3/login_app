import React from "react";
import { createStackNavigator, createAppContainer } from "react-navigation";
import Dashboard from "./Dashboard";
import Auth from "./Auth";
import { Platform, Dimensions, TouchableOpacity, Text } from "react-native";

const ADDITIONAL_TOP =
  Platform.OS === "ios" && Dimensions.get("window").height > 810 ? 50 : 5;

const AppNavigator = createStackNavigator(
  {
    auth: {
      screen: Auth,
      navigationOptions: () => ({
        headerTransparent: true,
        gesturesEnabled: false
      })
    },
    dashboard: {
      screen: Dashboard,
      navigationOptions: ({ navigation }) => ({
        headerTransparent: true,
        gesturesEnabled: false,

        headerLeft: null,
        headerStyle: {
          borderBottomWidth: 1,
          borderBottomColor: "white",
          height: Platform.OS === "android" ? 50 : 55,
          paddingBottom: 10,
          paddingTop: 10
        },
        headerRight: (
          <TouchableOpacity
            style={{ marginRight: 20 }}
            onPress={() => navigation.goBack()}
          >
            <Text style={{ color: "white", fontWeight: "500", fontSize: 16 }}>
              Logout
            </Text>
          </TouchableOpacity>
        )
      })
    }
  },
  {
    initialRouteName: "auth"
  }
);

export const AppContainer = createAppContainer(AppNavigator);
