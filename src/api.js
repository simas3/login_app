import axios from "axios";
import qs from "qs";
import { Alert, AsyncStorage } from "react-native";

export const login = (
  username,
  password,
  navigate,
  stopLoading,
  clearFields
) => {
  axios
    .post(
      "https://vidqjclbhmef.herokuapp.com/credentials",
      qs.stringify({
        username,
        password
      }),
      {
        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
      }
    )
    .then(({ data: { token } }) => {
      //   AsyncStorage.setItem("token", JSON.stringify(token));

      getUser(token, navigate, stopLoading, clearFields);
    })
    .catch(err => {
      stopLoading();
      Alert.alert(
        "Oops",
        "Wrong credentials. Please try again.",
        [{ text: "OK", onPress: () => {} }],
        {
          cancelable: false
        }
      );
    });
};

const getUser = (token, navigate, stopLoading, clearFields) => {
  axios
    .get("https://vidqjclbhmef.herokuapp.com/user", {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token
      }
    })
    .then(res => {
      stopLoading();
      clearFields();
      navigate("dashboard", { user: res.data });
    })
    .catch(err => {
      stopLoading();
      Alert.alert(
        "Oops",
        "Something went wrong.",
        [{ text: "OK", onPress: () => {} }],
        {
          cancelable: false
        }
      );
    });
};
