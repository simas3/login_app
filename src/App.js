import React, { Component } from "react";
import { View } from "react-native";
import { AppContainer } from "./Navigation";

// To see all the requests in the chrome Dev tools in the network tab.
// remove in PROD
XMLHttpRequest = GLOBAL.originalXMLHttpRequest
  ? GLOBAL.originalXMLHttpRequest
  : GLOBAL.XMLHttpRequest;

export default class App extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <AppContainer />
      </View>
    );
  }
}
